﻿
![](Aspose.Words.ec77404e-0510-4e05-84bf-226d8d119433.001.png)![](Aspose.Words.ec77404e-0510-4e05-84bf-226d8d119433.002.jpeg)







**Software Engineering**
## **Einsendeaufgabe 1: Prozessmodelle im Einsatz**
##

![](Aspose.Words.ec77404e-0510-4e05-84bf-226d8d119433.003.png)





**Prof. Dr. Ammar Memari**
![](Aspose.Words.ec77404e-0510-4e05-84bf-226d8d119433.004.png)


Vorgelegt von: Ahmed Madi

`                           `Ahmad Alzaiem


![](Aspose.Words.ec77404e-0510-4e05-84bf-226d8d119433.005.png)














Datum: 19.05.2022

![](Aspose.Words.ec77404e-0510-4e05-84bf-226d8d119433.006.png)

1. **Einleitung:**

Die Software-Prozessmodelle werden in den Verschiedenen Firmen getrennt oder zusammen verwendet, es wird ein Mix- Modelle zwischen Zweimodelle erstellt, sodass es dem Projekt und dem Unternehme optimaler passt, wie manche Web-Seiten erwähnen. 

In unserem Marktstudie wird zwei Modelle (Iterative Prozessmodelle genauer gesagt “Das V-Modell XT“ und Agile Prozessmodelle) erklärt diskutiert und am Ende ein Beispiel von der Praxis genannt.  Im Folgenden wird ein kurzer Überblick über die Prozessmodelle, die von uns betrachtet werden, eingeworfen. 

- Das **V-Modell XT** ist ein Vorgehensmodell zur Umsetzung von IT-Projekten, insbesondere zur Entwicklung der Softwaresystemen. Durch die Vorgabe von Ergebnissen und Abläufe wird die Arbeit von Projekten unterstützen, so dass zu keinem Zeitpunkt unnötige Arbeiten und möglichst auch keine Leerlaufzeiten entstehen.
- **Agile Prozessmodell** ist in der Praxis heute immer stark zu verbreiten. Agilität bedeutet, schrittweise vorzugehen, anstatt alle Teile eines Projekts präzise vorzubereiten. Teambeteiligung steht im Mittelpunkt des Ansatzes, Mitarbeiter sind also weitgehend selbstorganisiert und setzen sich eigene Aufgaben, während das Projektmanagement eher eine moderierende Rolle einnimmt.

1. **Das V-Modell**  

![](Aspose.Words.ec77404e-0510-4e05-84bf-226d8d119433.007.png)

*Abbildung 1: [https://www.scnsoft.de/blog/vorgehensmodelle-der-softwareentwicklung*](https://www.scnsoft.de/blog/vorgehensmodelle-der-softwareentwicklung)*


- Das V-Modell ist eine Erweiterung von dem Wasserfallmodell
- Das V-Modell ist ein Vorgehensmodell, bei dem alle Stufen nacheinander, also linear, durchlaufen werden. Aber für jede Phase werden entsprechende Testaktivitäten definiert. Dies ermöglicht einerseits eine effektivere Kontrolle der Softwarequalität und minimiert so das Projektrisiko. Andererseits ist Model V eines der teuersten und zeitaufwändigsten Modelle. Selbst wenn Fehler in Anforderungsspezifikation, Code und Architektur frühzeitig erkannt werden, kann die Implementierung von Änderungen während der Entwicklung trotz allem teuer und schwierig sein. Wie beim Wasserfallmodell werden am Anfang alle Anforderungen erfasst und können nicht verändert werden. 


- **Vorteile** 
- Phasen laufen vollständig in einer bestimmten richtigen Reihenfolge
- jede einzelne Phase wird dokumentiert am Ende jeder Phase ist ein Dokument erstellt.
- Der Entwicklungsprozess ist grundsätzlich sequentiell 
- Ist einfach, verständlich und benötigt wenig Managementaufwand


- **Nachteile** 
- Das Festlegen, Bestellen und Abschließen einer Etappe ist nicht immer sinnvoll 
- Die Dokumentation kann wichtiger sein als das System selbst
- Vielleicht werden Risikofaktoren zu wenig berücksichtigt

- **Anwendungsbereiche:** 

Elemente, die Unterbrechungen und Ausfallzeiten nicht akzeptieren können (z. B. medizinische Software, Fuhrparkmanagementsoftware für die Luftfahrt)

- **Ergebnisse:**

Das V-Modell wird in verschiedenen Bereichen eingesetzt, insbesondre in den sensiblen Bereichen, in den keinen Platz für Systemversagen ist. Es ist nützlich, wenn komplexe Systeme entwickelt werden, die auf natürliche Weise hierarchisch in Komponenten und Unterkomponenten unterteilt werden können.

Das V-Modell dient zu folgenden Punkten:  

- Minimierung der Projektrisiken
- Verbesserung und Gewährleistung der Qualität
- Verbesserung der Kommunikation zwischen allen Beteiligten
- Eindämmung der Gesamtkosten über den gesamten Projekt- und Systemlebenszyklus



1. **Agiler Prozessmodell** wird wie in dem folgenden Bild funktioniert.
## ![Agiles Prozessmanagement](Aspose.Words.ec77404e-0510-4e05-84bf-226d8d119433.008.jpeg)
*Abbildung 2**
- ## **Die Vorteile agiler Prozessmodell**
- Schneller Projektstart.
- Direkter Einfluss auf den Projektverlauf
- Effektivere Arbeitsabläufe
- Hohe Flexibilität
- Frühe Fehlererkennung 
- Effektive Ergebnisse, die auf Kunden- und Nutzerbedürfnisse zugeschnitten sind. 
- Risikominderung
- Hohe Kundenzufriedenheit
- Höhere Qualität der Ergebnisse

- **Die Nachteile agiler Prozessmodell:**
- Geringere Vorhersehbarkeit
- mehr Zeit und Engagement
- mehr Kommunikationsaufwand mit Kunden
- Mangel an notwendiger Dokumentation
- mehr Aufwand für die Projektteams

- **Anwendungsbereiche:** 
- Fast jedes Startup-Unternehmen, bei denen ein frühzeitiges Feedback der Endbenutzer notwendig ist.
- Die häufigsten mittelgroßen Projekte im Rahmen der individuellen Softwareentwicklung, bei denen detaillierte Softwareanforderungen auf Geschäftsanforderungen stützen.
- Große Projekte lassen sich leicht in kleinere Teile zerlegen und Schritt für Schritt angehen.

**Beispiel** für agiler Prozessmodell



![https://www.affinis.de/wp-content/uploads/2019/06/affinis-Prozesslandkarte-fuer-Energieversorgungsunternehmen.png](Aspose.Words.ec77404e-0510-4e05-84bf-226d8d119433.009.png)

*Abbildung 3**

Auf der obersten **Prozesscluster-Ebene** werden je nach den Gegebenheiten des Unternehmens Kategorien mit artverwandten Prozessen gebildet und diese mit einer eindeutigen Namenskonvention versehen, die an darunterliegende Ebenen vererbt wird.

Auf der zweiten **Prozess-Ebene** werden sämtliche Prozesse des jeweiligen Clusters gesammelt und mit einer weitergeführten Namenskonvention versehen. Die Prozess-Ebene dient hierbei als übergeordnete Kategorie für die nächste Ebene.

Die letzte Instanz bildet die **Teilprozess-Ebene,** auf dieser werden die gesammelten Prozesse in einzelne Arbeitsschritte zergliedert. Nur auf dieser Ebene wird eine logische Abfolge dieser einzelnen Schritte dargestellt.

Eine solche Prozesslandkarte bietet einen strukturierten Überblick über alle bestehenden Vorgänge in der Organisation und ist damit ein gutes Werkzeug, um einen umfassenden Ist-Zustand zu erheben, auf dem Maßnahmen zur Prozessoptimierung usw. aufsetzen können.

Nachdem die Prozesse eines beispielhaften Energieversorgers in der beschriebenen Prozesslandkarte definiert und kategorisiert wurden, wird im nächsten Schritt ein Pilotprozess zur Optimierung, ggf. zum Re-Design ausgewählt. Idealerweise handelt es sich dabei um einen Prozess mit Berührungspunkten zu Kunden des Unternehmens, da der Kundennutzen häufig im Fokus steht. Ein möglicher Beispielprozess ist der Prozess der Verbrauchsrechnung. Dieser Prozess basiert auf Scrum-Methode und wird der gesamte Prozess als Produkt, mitsamt Backlog der funktionalen Eigenschaften und eigenem Business-Case, definiert. Dem Produkt wird ein interner Product-Owner zugeordnet, der die Verantwortung für das Re-Design/die Optimierung trägt. Der Product-Owner wird hierbei von einem Team aus Fachleuten, dem Design-Team in Bezug auf den Rechnungslegungsprozess unterstützt. Der Methodik folgend wird in einzelnen Sprints von in diesem Fall 3 Wochen jeweils ein inkrementeller Baustein des Prozesses erarbeitet, bis ein sogenannter Minimal Viable Prozess – also ein Prozess, der alle Mindestanforderungen erfüllt, entwickelt ist. Dieser wird in weiteren Sprints immer weiterhin verfeinert und gemäß dem Feedback von internen und externen Stakeholdern angepasst. Die Länge der Sprints ist zu Anfang der Prozessoptimierung zu wählen und richtet sich nach den Gegebenheiten im Unternehmen (Auslastung der Mitarbeiter, Verfügbarkeit von Ressourcen…)

**Ergebnis:** Das Redesign hat die Fehlerquote bei der Abrechnung halbiert. 

**Literatur:** 

- [**https://projekte-leicht-gemacht.de/blog/projektmanagement/klassisch/v-modell/**](https://projekte-leicht-gemacht.de/blog/projektmanagement/klassisch/v-modell/)
- E-Book Das V-Modell XT : Grundlagen, Methodik und Anwendungen, Reinhard Höhn ; Stephan Höppner Berlin, Heidelberg : Springer Berlin Heidelberg, 2008 
- Agilität in Unternehmen: Eine praktische Einführung in SAFe® und Co.  Mario A Pfannstiel (HerausgeberIn); Werner Siedl (HerausgeberIn) ; Peter Steinhoff (HerausgeberIn) 1st ed. 2021.., Wiesbaden: Springer Fachmedien Wiesbaden: Imprint: Springer Gabler, 2021
- [**https://www.affinis.de/fachartikel/energiewirtschaft/prozessmanagement-bei-energieversorgern-herausforderungen-methoden-und-ein-blick-in-die-zukunft**](https://www.affinis.de/fachartikel/energiewirtschaft/prozessmanagement-bei-energieversorgern-herausforderungen-methoden-und-ein-blick-in-die-zukunft)
- [**https://www.springerprofessional.de/unternehmensprozesse/agile-methoden/der-weg-zum-agilen-prozessmanagement/15634924**](https://www.springerprofessional.de/unternehmensprozesse/agile-methoden/der-weg-zum-agilen-prozessmanagement/15634924)

5

